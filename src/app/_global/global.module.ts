import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserInfoComponent } from "./user-info/user-info.component";
import { UserAvatarComponent } from "./user-avatar/user-avatar.component";

const cmpArr = [
  UserInfoComponent,
  UserAvatarComponent
];

@NgModule({
  declarations: [
    ...cmpArr
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ...cmpArr
  ]
})
export class GlobalModule { }
