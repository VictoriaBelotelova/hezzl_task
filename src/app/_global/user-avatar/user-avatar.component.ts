import { Component, OnInit } from '@angular/core';
import { UserService } from "../../_core/services/user.service";

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {

  constructor(
    public user: UserService
  ) { }

  ngOnInit() {
  }

}
