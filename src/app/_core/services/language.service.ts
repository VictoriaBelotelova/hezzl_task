import { Injectable } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  public activeLang: string;
  constructor(
    private translate: TranslateService
  ) {
    this.getUserLang();
  }

  private getUserLang() {
    const user_lang = localStorage.getItem('user-lang');
    this.translate.addLangs(['en', 'ru']);
    const browserLang = user_lang ? user_lang : this.translate.getBrowserLang();
    this.changeUserLang(browserLang);
  }

  public changeUserLang(lang: string) {
    this.activeLang = lang;
    localStorage.setItem('user-lang', lang);
    this.translate.use(lang);
  }
}
