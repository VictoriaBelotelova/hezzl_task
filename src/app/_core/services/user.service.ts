import { Injectable } from '@angular/core';
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: User;
  constructor() {
    this.initUser();
  }

  private initUser() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  public getUser(): User {
    return this.user;
  }

  public setUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.user = user;
  }
}
