import { Injectable } from '@angular/core';
import {Observable, Subscriber, throwError} from "rxjs";
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  public login(form: any): Observable<any> {
    // заглушка
    if (form.Email === 'admin@admin.ru' && form.Password === '1234') {
      return new Observable<User>((subscriber: Subscriber<User>) => subscriber.next(this.getUserModel()));
    } else {
      return throwError({})
    }
  }

  public logout() {
    localStorage.removeItem('user');
  }

  private getUserModel(): User {
    return {
      Name: 'Рамин',
      Surname: 'Алиев',
      Username: 'ramin@hezzl.ru'
    }
  }
}
