import { Component, OnInit } from '@angular/core';
import { langs } from "./langs";
import { LanguageService } from "../_core/services/language.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../_core/services/auth.service";
import { UserService } from "../_core/services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginGroup: FormGroup;
  isErrorForm = false;
  langsArr = langs.filter(i => !i.disable);
  constructor (
    public langSrv: LanguageService,
    private fb: FormBuilder,
    private auth: AuthService,
    private user: UserService,
    private router: Router
  ) {
    this.auth.logout();
    this.initForm();
  }

  ngOnInit() {
  }

  public login() {
    this.isErrorForm = false;
    if (this.loginGroup.valid) {
      this.auth.login(this.loginGroup.value).subscribe(res => {
        this.user.setUser(res);
        this.router.navigate(['']);
      }, err => {
        this.isErrorForm = true;
      });
    } else {
      this.isErrorForm = true;
    }
  }

  public validateField(control: string): boolean {
    if (this.loginGroup.get(control).dirty) {
      return this.loginGroup.get(control).hasError('required') || this.loginGroup.get(control).hasError('pattern');
    } else return false;
  }

  private initForm() {
    this.loginGroup = this.fb.group({
      Password: ['', [Validators.required]],
      Email: ['', [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,200})+$/)]]
    });
  }
}
