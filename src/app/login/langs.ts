export const langs = [
  {
    'id': 10,
    'name': 'EN',
    'i18n': 'en',
    'disable': false
  },
  {
    'id': 20,
    'name': 'Русский',
    'i18n': 'ru',
    'disable': false
  }
];
