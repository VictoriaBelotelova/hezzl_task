import { Component, OnInit } from '@angular/core';
import { menus } from "./menu-element";
import { Router } from "@angular/router";

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  public menus = menus.filter(it => !it.disable);
  checked: boolean;
  roles = [];
  constructor(
    private router: Router,
  ) {
    this.checkActiveRoute();
  }

  ngOnInit() {
  }

  public openMenu(id: number): void {
    this.menus.forEach(item => item.open = false);
    const menuItem = this.menus.find(item => item.id === id);
    menuItem.open = true;
    this.router.navigate([menuItem.link]);
  }

  private checkActiveRoute() {
    this.menus.forEach(item => item.open = false);
    this.menus.forEach(item => {
      if (item.link === this.router.url.slice(1)) {
        item.open = true;
      }
    });
  }
}
