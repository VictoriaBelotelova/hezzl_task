export const menus = [
  {
    'id': 10,
    'name': 'MENU.COMPANIES',
    'icon': 'fa-book-reader',
    'link': 'companies',
    'open': false,
    'role': [],
    'disable': false
  },
  {
    'id': 20,
    'name': 'MENU.APPEALS',
    'icon': 'fa-heart',
    'link': 'appeals',
    'open': false,
    'role': [],
    'disable': false
  },
];
