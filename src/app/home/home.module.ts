import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from "./components/side-menu/side-menu.component";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { TranslateModule } from "@ngx-translate/core";
import { GlobalModule } from "../_global/global.module";

@NgModule({
  declarations: [
    SideMenuComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    TranslateModule,
    GlobalModule
  ]
})
export class HomeModule { }
