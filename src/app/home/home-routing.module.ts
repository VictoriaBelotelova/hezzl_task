import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home.component";

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      { path: '', loadChildren: './empty-page/empty-page.module#EmptyPageModule' },
      { path: 'companies', loadChildren: './empty-page/empty-page.module#EmptyPageModule' },
      { path: 'appeals', loadChildren: './empty-page/empty-page.module#EmptyPageModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
