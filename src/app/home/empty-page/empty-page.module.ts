import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { EmptyPageComponent } from "./empty-page.component";
import { TranslateModule } from "@ngx-translate/core";

const routes: Routes = [
  { path: '', component: EmptyPageComponent }
];

@NgModule({
  declarations: [
    EmptyPageComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(routes),
  ]
})
export class EmptyPageModule { }
