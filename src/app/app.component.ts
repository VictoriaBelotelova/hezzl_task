import { Component } from '@angular/core';
import { UserService } from "./_core/services/user.service";
import { LanguageService } from "./_core/services/language.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserService, LanguageService]
})
export class AppComponent {
  constructor(
    private lang: LanguageService
  ) {}
}
